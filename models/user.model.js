const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    name: {type: String, required: true, max: 100},
    mail: {type: String, required: false, max: 100},
    age: {type: Number, required: true}
});
UserSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});



// Export the models
module.exports = mongoose.model('User', UserSchema);