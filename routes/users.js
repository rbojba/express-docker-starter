var express = require('express');
var router = express.Router();
const userController = require('../controllers/user.controller');



/* GET users listing. */
router.get('/', userController.getAll);

/* GET users listing. */
router.get('/:userId', userController.getOne);

/* POST users listing. */
router.post('/', userController.addOne);

/* PUT users listing. */
router.put('/', userController.addOne);

/* DELETE users listing. */
router.delete('/:userId', userController.deleteOne);

module.exports = router;
