const User = require('../models/user.model');

//Simple version, without validation or sanitation
exports.getAll = function (req, res) {
    // res.send('Greetings from the Test controller!');
    res.setHeader('Content-Type', 'application/json');
    //res.send(JSON.stringify(User.find()));
    User.find().lean().exec(function (err, users) {
        return res.json(users);
    });
};

exports.getOne = function (req, res) {
    // res.send('Greetings from the Test controller!');
    res.setHeader('Content-Type', 'application/json');
    //res.send(JSON.stringify(User.find()));
    User.findById(req.params['userId']).exec(function (err, user) {
        return res.json(user);
    });
};

exports.addOne = function (req, res) {
    // res.send('Greetings from the Test controller!');
    res.setHeader('Content-Type', 'application/json');

    const user = new User();
    user.name = req.body.name;
    user.age = req.body.age;

    user.save(function (err) {
        console.error(err);
        if (err) {
            let errorResponse = {
                error: "Cannot add user",
                errorDetail: err.toString()
            };
            res.json(errorResponse);
        } else return res.json(user);
    });
    //res.send(JSON.stringify(User.find()));
};

exports.deleteOne = function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    User.findByIdAndDelete(req.params['userId'], function (err) {
        console.error(err);
        if (err) {
            let errorResponse = {
                error: "Cannot delete user",
                errorDetail: err.toString()
            };
            res.json(errorResponse);
        } else return res.json({status: 'OK'});
    });

}